# herokuworker - Open Education Tagger

* Full project documentation:
https://github.com/programmieraffe/openeducationtagger *

This worker essentially runs the [openeducationtaggerclass.js](https://gitlab.com/programmieraffe/openeducationtagger/-/blob/master/assets/scripts/openeducationtaggerclass.js) (which was built for web browser usage (ES6)) on heroku.com with a node worker (via command line). Therefore it is possible to use the free addon Heroku Scheduler to run it every minute/hour/day. We use [esm](https://www.npmjs.com/package/esm) (`node -r esm bin/worker.js`), "brilliantly simple, babel-less, bundle-less ECMAScript module loader".

## Setup a new worker (without fork)

1. create own new empty git repository
2. `git remote add upstream https://gitlab.com/programmieraffe/openeducationtagger-herokuworker.git`
3. Pull remote code in: `git pull upstream master`
4. submit these changes `git push origin master` (to new repository)
5. openeducationtagger-herokuworker uses a submodule (openeducationtagger), because they share code - this must be initialized to be pulled in
```
git submodule update --init
```
6. Time to create a heroku app: `heroku create YOURAPPNAME --region eu?`
7. Add config values to .env (duplicate .env example)
8. Test locally: `heroku local worker` (run `npm install` beforehand)
9. Set config to heroku production:

```
sed 's/#[^("|'')]*$//;s/^#.*$//' .env | \
 xargs heroku config:set
```
10. Use `heroku config` to check wheter this worked

11. Check if it works on heroku production as well:
12. `git push heroku master`
13. connect to heroku bash `heroku run bash`
14. run `node -r esm bin/worker.js` on bash
15. Add heroku scheduler add-on `heroku addons:create scheduler:standard`
and create a job with `node -r esm bin/worker.js` (via `heroku addons:open scheduler`)

### 2DO: How to update?

1. Pull submodule changes from openeducationtagger?
1.1 Is this automatically done on heroku production?
2. Merge upstream remote changes from openeducationtagger-herokuworker?


### Debug and troubleshooing


------------------------
Updates can be pulled in now...


2DO: What about the submodule? (Not pulled in via upstream?)

## Setup a scheduled worker

- this is a worker (runs with node, but it is just executing the ES6 JS class from the browser tool)
- uses git submodule to get same class from spreadsheet2index online browser tool:
https://gitlab.com/programmieraffe/openeducationtagger-spreadsheet2index

## How to set it up:

git init
git add .
git commit -m "update"
heroku create YOURAPPNAME --region eu
# heroku create automatically creates a git remote entry, so we can push to that instance:
git push heroku master
# the first push will result in -> Node.js app detected, so heroku instance is now ready for node


### Setup config

1.Dashboard heroku.com

2. Advanced
.env (copy from .env.example)
```
OET_SPREADSHEET_ID=
OET_SPREADSHEET_SHEET_ID=od6
OET_ELASTICSEARCH_HOSTNAME=
OET_ELASTICSEARCH_INDEX=
OET_ELASTICSEARCH_AUTH_STRING_WRITE=
```

Fill the values and copy them to the heroku production instance:
```
sed 's/#[^("|'')]*$//;s/^#.*$//' .env | \
 xargs heroku config:set
```

### 2DO: Test it

Test locally:
npm install
git submodule update --init
heroku local worker

# test it with one-off bash:
heroku run bash

# on bash
node -r esm bin/worker.js
# check console.log

! Important: esm -> ES6!

// 2DO: SHOW HEROKU LOGS:

### 2DO: Setup scheduled worker / cron job:



## 2DO: How to update submodule?

- update submodule via sourcetree e.g.
- commit will be generated for main repo
commit & push to heroku

Submodule renaming with heroku:
https://stackoverflow.com/questions/31545902/unable-to-push-to-heroku-after-removing-submodule

--- OLD DOCS:

https://medium.com/@pativancarrasco/why-your-es6-syntax-doesnt-work-in-node-js-and-how-to-fix-it-161f0708f1ad


!!

https://timonweb.com/tutorials/how-to-enable-ecmascript-6-imports-in-nodejs/


npm install --save esm


export default:
https://medium.com/@etherealm/named-export-vs-default-export-in-es6-affb483a0910
