
// https://www.kaplankomputing.com/blog/tutorials/javascript/understanding-imports-exports-es6/
// https://javascript.info/import-export
// default import, we don't have to name interval
//https://medium.com/@etherealm/named-export-vs-default-export-in-es6-affb483a0910

console.log('##### Start worker ######');

// server - client codesharing (run in webbrowser as well as node)

// load JS class (provided by git submodule)
import OpenEducationTagger from './../openeducationtagger/assets/scripts/openeducationtaggerclass';

// workaround for js functions which aren't defined in node
global.btoa = require('btoa');
global.fetch = require("node-fetch");
global.sanitizeHtml = require('sanitize-html');

// console.log('test process.env.TEST ', process.env.TEST);
// console.log('import', typeof OpenEducationTagger);
// console.log('btoa', typeof btoa);

// create new class instance (class uses ES6 'export default')
const oet = new OpenEducationTagger();
// console.log('WORKS', typeof oet);

// get config values from heroku env/config vars
const configObject = {
  "oet_spreadsheet_id": process.env.OET_SPREADSHEET_ID,
  "oet_spreadsheet_sheet_id": process.env.OET_SPREADSHEET_SHEET_ID,
  "oet_elasticsearch_hostname":process.env.OET_ELASTICSEARCH_HOSTNAME,
  "oet_elasticsearch_index":process.env.OET_ELASTICSEARCH_INDEX,
  "oet_elasticsearch_auth_string_write":process.env.OET_ELASTICSEARCH_AUTH_STRING_WRITE
};

// 2DO: validate here or in class?
// quick check to save time
if(process.env.OET_ELASTICSEARCH_AUTH_STRING_WRITE == undefined || process.env.OET_ELASTICSEARCH_AUTH_STRING_WRITE == ''){
  console.log('ERROR: No auth string provided, stopping script');
}else{
  oet.setConfig(configObject);
  oet.startSync();
}

// oet.startSync();

// https://erickar.be/blog/running-a-cron-job-with-node-js-and-heroku

// https://stackoverflow.com/a/35002367

// https://www.jaygould.co.uk/2017-11-14-cloud-deployment-heroku-node-babel/

// https://gist.github.com/bitgord/245535acdfb39f489913c1cf3e6fe6af


// https://www.kaplankomputing.com/blog/tutorials/javascript/understanding-imports-exports-es6/
